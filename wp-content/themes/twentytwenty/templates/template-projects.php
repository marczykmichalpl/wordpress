<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 * 
* Template name: Szablon projektów
* Template Post Type: post, page
*/

$args = array (
    'post_type' => 'projects',
    'posts_per_page' => -1,
    'orderby' => 'date',
    'order'   => 'ASC',
  );
$query = new WP_Query( $args );
   
get_header();

get_template_part( 'template-parts/entry-header' );
?>

<main id="site-content" role="main">
    <section class="projects-list">   
        <?php
        if( $query->have_posts() ){
            while( $query->have_posts() ) {           
                $query->the_post(); ?> 
                    <?php include locate_template('template-parts/project-tile.php'); ?>
        <?php
            }
        }
        wp_reset_query();
        ?>
     </section>
</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>