<div class="project-tile">
    <?php
        $thumbId = get_post_thumbnail_id( get_the_id() );
        $thumbSrc = wp_get_attachment_image_src($thumbId, 'medium');
    ?>
    <?php if( $thumbSrc ): ?>
        <div class="image-wrap">
            <img class="image lazy" src="<?php echo get_template_directory_uri(); ?>/dist/img/placeholder.png" data-src="<?php echo $thumbSrc[0]; ?>" data-srcset="<?php echo $thumbSrc[0]; ?>" alt="<?php echo get_the_title(); ?>">
        </div>
    <?php endif; ?>
    <h5 class="title"><?php echo get_the_title(); ?> </h5>
    <p class="short-desc"><?php echo get_the_excerpt(); ?> </p>
    <a class="btn" href="<?php echo get_page_link(); ?>">zobacz więcej</a>
</div>